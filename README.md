Este proyecto es creado como parte de una prueba tecnica para la empresa Codigo fuente.

Ha sido desarrollado con el framework Fractal / Webpack, para poder correr este proyecto:

1. Instalar Node
2. Correr 'npm install'
3. Correr 'npm fractaldev'

4. Compilar 'npm fractal:build'