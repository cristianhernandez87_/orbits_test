export function menuScroll() {

    $(window).scroll(function(event) {
        var altura = $(window).height();
        var scroll = $(window).scrollTop();
        var alturaNecesitada = altura - 100;
        if (scroll >= alturaNecesitada) {
            $("body").addClass("top--scroll");
            //console.log(altura);
        } else {
            $("body").removeClass("top--scroll");
        }
    });  
}