import $ from 'jquery';
import 'bootstrap';
import 'daterangepicker';
import 'flickity';
var flickity = require('flickity');
import 'flickity-fullscreen';

/**
 * Styles
 */
import '../scss/index.scss';

/**
 * Modules
 */

import { menu } from '../components/comp/c-hamburguesa/c-hamburguesa';
import { video } from '../components/boards/b-video/b-video';
import { options } from '../components/sections/s-options/s-options';

/**
 * Init
 */
 
menu();
video();
options();