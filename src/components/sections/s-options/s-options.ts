export function options() {
    if(document.getElementById("options-open")){
	    document.getElementById("options-open").addEventListener("click", function() {
	        optionsMenu();
	    });
    }
    
    function optionsMenu() {
        if (document.body.classList.contains('open-options')) {
            document.body.classList.remove('open-options');
        } else {
            document.body.classList.add('open-options');
        };
    }
}