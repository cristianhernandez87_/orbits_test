export function menu() {
    if(document.getElementById("menu-open")){
	    document.getElementById("menu-open").addEventListener("click", function() {
	        moveMenu();
	    });
    }
    
    function moveMenu() {
        if (document.body.classList.contains('open-menu')) {
            document.body.classList.remove('open-menu');
        } else {
            document.body.classList.add('open-menu');
        };
    }
}