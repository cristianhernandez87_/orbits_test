export function video() {
    var forSelect = $('.b-video');
    
    forSelect.bind('click', function(){
        var e = $(this);

        if(e.hasClass('b-video--checked')){
            e.removeClass('b-video--checked');
        } else {
            e.addClass('b-video--checked');
        }
    });
}